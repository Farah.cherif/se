/*****************************
 * Mini projet SE, 1 IDL Gr2
 * Par Farah ET Omar
 ****************************/

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <errno.h>
#include <glob.h>

#define TAILLE_BUFFER 250

char *arg_list[32],*arg_list2[32];
char buffer[TAILLE_BUFFER];

void str_replace(char *chaine,char* recherche,char *remplace)
{
int nbre=0;

    char *p=chaine;
    char *tmp=strstr(p,recherche);
    while (tmp!=NULL)
    {
        ++nbre;
        p=tmp+strlen(recherche);
        tmp=strstr(p,recherche);
    }
    if (nbre>0)
    {
        char *chaine_copie=malloc(strlen(chaine)-(strlen(recherche)*nbre)+(strlen(remplace)*nbre)+1);
        chaine_copie[0]='\0';
        char *p=chaine;
        char *tmp=strstr(p,recherche);
        while (tmp!=NULL)
        {
            strncat(chaine_copie,p,tmp-p);
            strcat(chaine_copie,remplace);
            p=tmp+strlen(recherche);
            tmp=strstr(p,recherche);
        }
        strcat(chaine_copie,p);
        strcpy(chaine,chaine_copie);
        free(chaine_copie);
    }
}

void creation_liste_arguments(char *arguments[32],char *commande)
{
char *strtokadr;
int  boucle,increment;

    for (boucle=0;boucle<32;++boucle)
    {
        arguments[boucle]=NULL;
    }
    char *p=strdup(commande);
    char *tmp=strtok_r(p," ",&strtokadr);
    increment=0;
    while (tmp!=NULL)
    {
        arguments[increment]=strdup(tmp);
        ++increment;    
        tmp=strtok_r(NULL," ",&strtokadr);    
    }
    free(p);
}

void liberation_arguments(char *arguments[32])
{
int increment=0;
    while (arguments[increment]!=NULL)
    {
        free(arguments[increment]);
        increment++;
    }
}

void traitement_joker(char *arguments[32])
{
char *arg_list_tmp[32];

    int increment=0;
    int increment_tmp=0;
    while (arguments[increment]!=NULL)
    {
        char *tmp=strstr(arguments[increment],"*");
        if (tmp!=NULL)
        {
            glob_t g;
            int retour_glob=glob(tmp,0,NULL,&g);
            if (retour_glob==0)
            {
                int boucle;
                for (boucle=0;boucle<g.gl_pathc;++boucle)
                {
                    arg_list_tmp[increment_tmp]=strdup(g.gl_pathv[boucle]);
                    ++increment_tmp;
                }
                free(arguments[increment]);
            }
            else
            {
                arg_list_tmp[increment_tmp]=arguments[increment];
                ++increment_tmp;
            }
            globfree(&g);
        }
        else
        {
            arg_list_tmp[increment_tmp]=arguments[increment];
            ++increment_tmp;
        }
        ++increment;
    }
    arg_list_tmp[increment_tmp]=NULL;
    increment=0;
    while (arg_list_tmp[increment]!=NULL)
    {
        arguments[increment]=arg_list_tmp[increment];
        ++increment;
    }
    arguments[increment]=NULL;
}

char *scan_redirection_entrante(char *arguments[32])
{
char *redirection=NULL;
int  increment=0;

    while (arguments[increment]!=NULL)
    {
        if (strcmp(arguments[increment],"<")==0)
        {
            redirection=arguments[increment+1];
            free(arguments[increment]);        
            while (arguments[increment+2]!=NULL)
            {
                arguments[increment]=arguments[increment+2];
                ++increment;
            }
            arguments[increment]=NULL;
        }
        ++increment;
    }
    return redirection;
}

char *scan_redirection_sortante(char *arguments[32])
{
char *redirection=NULL;
int  increment=0;

    while (arguments[increment]!=NULL)
    {
        if (strcmp(arguments[increment],">")==0)
        {
            redirection=malloc(strlen(arguments[increment+1])+1);
            redirection[0]='w';
            redirection[1]='\0';
            strcat(redirection,arguments[increment+1]);
            free(arguments[increment]);    
            free(arguments[increment+1]);    
            while (arguments[increment+2]!=NULL)
            {
                arguments[increment]=arguments[increment+2];
                ++increment;
            }
            arguments[increment]=NULL;
        }
        else if (strcmp(arguments[increment],">>")==0)
        {
            redirection=malloc(strlen(arguments[increment+1])+1);
            redirection[0]='a';
            redirection[1]='\0';
            strcat(redirection,arguments[increment+1]);
            free(arguments[increment]);    
            free(arguments[increment+1]);    
            while (arguments[increment+2]!=NULL)
            {
                arguments[increment]=arguments[increment+2];
                ++increment;
            }
            arguments[increment]=NULL;
        }
        ++increment;
    }
    return redirection;
}

void traitement_ligne()
{
char *cmd1,*cmd2;
char *fichier_redirection_sortante,*fichier_redirection_entrante;
char *fichier_redirection_sortante2,*fichier_redirection_entrante2;
int pipefd[2];

    cmd2=NULL;
    str_replace(buffer,"\n","");
    str_replace(buffer," <","<");
    str_replace(buffer,"< ","<");
    str_replace(buffer,"<"," < ");
    str_replace(buffer," >",">");
    str_replace(buffer,"> ",">");
    str_replace(buffer,">"," > ");
    str_replace(buffer," >  > "," >> ");
    str_replace(buffer,"| ","|");
    str_replace(buffer," |","|");
    str_replace(buffer,"|"," | ");
    char *tmp=strstr(buffer," | ");
    if (tmp!=NULL)
    {
        cmd1=strndup(buffer,strlen(buffer)-strlen(tmp));
        cmd2=strdup(tmp+3);
    }
    else
    {
        cmd1=strdup(buffer);
    }
    creation_liste_arguments(arg_list,cmd1);
    traitement_joker(arg_list);
    fichier_redirection_sortante=scan_redirection_sortante(arg_list);
    fichier_redirection_entrante=scan_redirection_entrante(arg_list);
    if (cmd2!=NULL)
    {
        creation_liste_arguments(arg_list2,cmd2);
        traitement_joker(arg_list2);
        fichier_redirection_sortante2=scan_redirection_sortante(arg_list2);
        fichier_redirection_entrante2=scan_redirection_entrante(arg_list2);
    }        
    pipe(pipefd);
    pid_t process=fork();
    if (process==0)
    {
        if (cmd2!=NULL) 
        {
            dup2(pipefd[1],STDOUT_FILENO);
        }
        if (fichier_redirection_entrante!=NULL)
        {
            int handler=(int)freopen(fichier_redirection_entrante,"r", stdin);
            if (handler==-1) 
            {
                fprintf(stderr,"%s\n",strerror(errno));
                exit(0);
            }
        }
        if (fichier_redirection_sortante!=NULL)
        {
            char* type_redirection=strndup(fichier_redirection_sortante,1);
            int handler=(int)freopen(fichier_redirection_sortante+1,type_redirection, stdout);
            if (handler==-1) 
            {
                fprintf(stderr,"%s\n",strerror(errno));
                exit(0);
            }
            free(type_redirection);
        }
        int retour=execvp(arg_list[0],arg_list);
        if (retour==-1) printf("%s\n",strerror(errno));
        exit(0);
    }
    else
    {
        wait(&process);
    }
    if (cmd2!=NULL)
    {
        pid_t process2=fork();
        if (process2==0)
        {
            dup2(pipefd[0],STDIN_FILENO);
            if (fichier_redirection_entrante2!=NULL)
            {
                int handler=(int)freopen(fichier_redirection_entrante2,"r", stdin);
                if (handler==-1) 
                {
                    fprintf(stderr,"%s\n",strerror(errno));
                    exit(0);
                }
            }
            if (fichier_redirection_sortante2!=NULL)
            {
                char* type_redirection=strndup(fichier_redirection_sortante,1);
                int handler=(int)freopen(fichier_redirection_sortante2+1,type_redirection, stdout);
                if (handler==-1) 
                {
                    fprintf(stderr,"%s\n",strerror(errno));
                    exit(0);
                }
                free(type_redirection);
            }
            int retour=execvp(arg_list2[0],arg_list2);
            if (retour==-1) printf("%s\n",strerror(errno));
            exit(0);
        }
        else
        {
            wait(&process2);
        }
    }
        
    if (cmd2!=NULL)
    {
        if (fichier_redirection_sortante2!=NULL) free(fichier_redirection_sortante2);
        if (fichier_redirection_entrante2!=NULL) free(fichier_redirection_entrante2);
        liberation_arguments(arg_list2);
        free(cmd2);
        cmd2=NULL;
    }
    if (fichier_redirection_entrante!=NULL) free(fichier_redirection_entrante);
    if (fichier_redirection_sortante!=NULL) free(fichier_redirection_sortante);
    liberation_arguments(arg_list);
    free(cmd1);
}

int main(int argc,char *argv[],char *arge[])
{
    if (argc>1)
    {
        printf("traitement mode batch en cours\n");
        FILE *fichier=fopen(argv[1],"r");
        if (fichier==NULL)
        {
            fprintf(stderr,"%s: %s\n",argv[1],strerror(errno));
            exit(EXIT_FAILURE);
        }
        while (fgets(buffer,150,fichier)!=NULL)
        {
            if (buffer[0]!='#')
            {
                traitement_ligne();
            }
        }
        exit(0);
    }
    else  // mode interactif
    {
        while(1)
        {
            printf("Shell farah et omar : ");
            fgets(buffer,150,stdin);
            buffer[strlen(buffer)-1]='\0';
            if (strcmp("exit",buffer)==0)
            {
                exit(0);
            }
            traitement_ligne();
        }
    }
}
